/**

Users
Server side functionality

**/

/**************************************************************************************/
/** Collection **/

Users.extendCollection({

	/** Get **/

	getByEmail: function (_email) {
		return Users.collection.findOne({
			'emails.address': _email
		});
	},

	getSamlUser: function (_idp, _userId) { // Deprecated, use getServiceUser
		return Users.collection.findOne({
			'services.saml.userId': _userId,
			'services.saml.idp': _idp,
		});
	},

	getUsersBySubscription: function (subject) {
		return Users.collection.find({
			subscriptions: subject
		}).fetch();
	},

	getServiceUser: function (_service, _authData) {
		var filter = {};
		filter['services.'+_service+'.id'] = _authData.id;

		return Users.collection.findOne(filter);
	},

	/** Generate Unique Username From String **/
	// we use a similar method to Collections.mixins.codename from the collections-base package

	generateUsernameFromString: function (_string) {
		var username = _string.toLowerCase().replace(/[\t\r\n\s]+/g, "-").replace(/[^a-zA-Z0-9_-]/g, "");

		var num = 0;
		var tryUsername;

		while (true) {
			var tryUsername = username + (num == 0 ? '' : '-' + num);

			if (!Users.getByUsername(tryUsername)) break;

			num++;
		}

		return tryUsername;
	}

});

/* Index */

Users.collection._ensureIndex({
	username: 1,
}, {unique: true});

Users.collection._ensureIndex({
	'emails.address': 1,
}, {unique: true});

/**************************************************************************************/
/** Document **/

Users.extendDocument({

	/* Set Name (=> push a codename) */

	setName: function (_name) {
		Users.collection.update(this._id, {
			$set: {'profile.name': _name}
		});
	},

	/** Representations **/

	basicRepresenation: function () {
		return {
			_id: this._id,
			username: this.username
		}
	},

	/** Groups **/

	addToGroup: function (_group_id) {
		if (this.groups) {
			if (_.indexOf(this.groups, _group_id) != -1) return;
		}

		Meteor.users.update(this._id, { // note even if groups does not exist, meteor $push creates it
			$push: {groups: _group_id}
		});

		// in place update
		this.groups = this.groups || [];
		this.groups.push(_group_id);
	},
	removeFromGroup: function (_group_id) {
		if (!this.groups) return;

		Meteor.users.update(this._id, {
			$pull: {groups: _group_id}
		});

		// in place update
		this.groups = _.without(this.groups, _group_id);
	},
	isInGroup: function (_group_id) {
		if (!this.groups) return false;

		return (_.indexOf(this.groups, _group_id) == -1 ? false : true);
	},

	/* Email */ // TODO: Implement email address verification
	// defaultEmail is integrated at the shared level (useful for the logged in user)

	hasEmail: function (_email) {
		if (!this.emails) return false;

		return _.find(this.emails, function (email) {
			return email.address == _email ? true : false;
		});
	},
	pushEmail: function (_email, _verified) {
		if (typeof(_verified) === "undefined") _verified = false;
		if (this.hasEmail(_email)) return true;

		try {
			// It's possible that another user already has this email
			// As emails.address is a primary key field - need to try catch to detect this clash/error
			// NOPE: This isn't the case, that it triggered an error, we need to actually check the user does not already have this email ourselves (see above)

			Users.collection.update(this._id, {
				$push: {emails: {address: _email, verified: _verified}}
			});
		} catch (err) {
			return false;
		}

		return true;
	},

	// sendEmail: [DEPRECATED - Should send emails by instantiating an instance of TemplateEmail or by sending via an external templating service like Campaign Monitor]
	// Sends an email directly to the user 

	sendEmail: function (subject, body, _type) {
		var email = this.defaultEmail();
		if (!email) return false;

		try {
			Email.send({
				to: email,
				from: HCConfig.emailFrom,
				subject: subject,
				html: body,
				headers: {
					'X-CMail-GroupName': _type,
				}
			});
		} catch (err) {
			return false;
		}

		return true;
	},

	// newEmail:
	// Gets a new email object which is addressed to this user, setup is then continued and send ordered in the calling code

	newEmail: function (subject, from) {
		var email = this.defaultEmail();
		if (!email) return null;

		return new TemplateEmail(email, subject, from);
	},

	/** Internal Data **/
	// Internal data are key value flags we can set on the user collection (and possibly that we could abstract to others) which make it easy to record flags and us processing certain options relating to them

	getInternals: function (key, _default) {
		return this.getSubfield('internals', key, _default);
	},

	// getInternal:
	// deprecated for getInternals

	getInternal: function (variable) { return this.getInternals(variable); },

	setInternals: function (values) {
		this.setSubfields('internals', values);
	},
	setInternal: function (variable, value) {
		var set = {}
		set[variable] = value;

		this.setInternals(set);
	},

	/** Forgot Password **/

	createPasswordResetToken: function () {
		// Taken from https://github.com/meteor/meteor/blob/master/packages/accounts-password/password_server.js#L377
		// We don't use the built in Meteor method as that requires us to use their template system

		var token = Random.secret();
		var when = new Date();
		var tokenRecord = {
			token: token,
			when: when,
			email: this.defaultEmail()
		}

		Users.collection.update(this._id, {
			$set: {
				"services.password.reset": tokenRecord
			}
		});

		return token;
	},

	sendPasswordResetEmail: function () {
		return EmailHooks.call('Password Reset', {
			user_id: this._id,
			token: this.createPasswordResetToken()
		});
	},

});

Meteor.methods({

	userByEmail_sendPasswordResetEmail: function (_email) {
		check(_email, String);

		var user = Users.getByEmail(_email);
		if (!user) throw new Meteor.Error(404, "User not found");

		if (!user.sendPasswordResetEmail()) throw new Meteor.Error(403, "Failed to send password reset email");

		return true;
	},

});

EmailHooks.register('Password Reset', function (data) {
	var user = Users.getById(data.user_id);

	var email = user.newEmail('HowCloud: Reset Your Password');
	if (!email) return false;

	email.setType('Password Reset');

	email.setContent([
		'Hi '+user.firstName()+',<br /><br />',
		'We received a request to change the password for your HowCloud account:<br /><br />',
		'<a class="button button-large" href="'+HC.web_url()+'reset/'+user._id+'/'+data.token+'">Click to Change Your Password</a><br /><br />',
		'If you did not make this request - please ignore this email.<br /><br/>',
		'~ HowCloud'
	].join(''));

	return email.send();
});

/**************************************************************************************/
/** Publications **/

/** Profiles **/

Meteor.publish("User_Profile_byId", function (_user_id) {
	return Meteor.users.find(_user_id, {
		fields: {'username': 1, 'type': 1, 'profile': 1, 'codename': 1, 'linkMethods': 1} // link methods are what allow us to login to an institutional saml account from a profile, for example
																						 // linkMethods are set for users who are really institutions, just easier to give them a user account for now
	});
});

Meteor.publish("User_Name_byId", function (_user_id) {
	return Meteor.users.find(_user_id, {
		fields: {'username': 1, 'type': 1, 'profile.name': 1, 'profile.guest': 1}
	});
});

// User_Profile_byIdentifier
// Used by profile pages to fetch a user's profile by either username or _id

Meteor.publish("User_Profile_byIdentifier", function (_identifier) {
	var projection = {
		fields: {'username': 1, 'type': 1, profile: 1, linkMethods: 1} // need to include codename to be able to query by identifier again on the client side
	};

	return Users.getByIdentifier_cursor(_identifier, projection);
});

/** This User Data **/

Meteor.publish("ThisUser_Data", function (_user_id) {
	// publishes extended data for the logged in user [automatically subscribed by client for current logged in user, see user_client.js]

	if (!this.userId) return null;
	if (this.userId != _user_id) return null;

	return Meteor.users.find(this.userId, {
		fields: {'services': 0, 'internals': 0} // we define fields negatively - ie publish everything except services and internals
	});
});

/** This User Perms **/

Meteor.publish("ThisUser_Perm", function (_user_id, permDescriptor) { 
	if (!this.userId) return null;
	if (this.userId != _user_id) return null;

	var user = Meteor.users.getById(this.userId);
	var groups = user.groups || [];

	return [
		Groups.collection.find({
			_id: {$in: groups}, // note this publication is not reactive to changes in user groups - they'd have to refresh for that
			perms: permDescriptor
		},
		{
			/*fields: {
				'perms.$': permDescriptor
			}*/
			fields: {perms: 1}
		})
	];
});

Meteor.publish("ThisUser_PermMatch", function (_user_id, partialDescriptor) {
	if (!this.userId) return null;
	if (this.userId != _user_id) return null;
	var user = Meteor.users.getById(this.userId);
	
	var groups = user.groups || [];

	return [
		Groups.collection.find({
			_id: {$in: groups}, // note this publication is not reactive to changes in user groups - they'd have to refresh for that
			perms: {$elemMatch: partialDescriptor}
		},
		{
			/*fields: {
				'perms': {$elemMatch: partialDescriptor}
			}*/
			fields: {perms: 1}
		})
	];
});

/** This User Groups **/

Meteor.publish("ThisUser_Groups", function (_user_id) { // todo: move to a thisUserData publication ???
	if (!this.userId) return null;
	if (this.userId != _user_id) return null;

	return Meteor.users.find(this.userId, {
		fields: {groups: 1}
	});
});