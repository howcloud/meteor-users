Package.describe({
	name: 'howcloud:users',
	summary: "Creates and exports a basic user collection using collection-base which wraps the default Meteor.users collection"
});

Package.on_use(function (api) {
	
	/* Package Dependencies */

	api.use('underscore');
	api.use('ddp');
	api.use('mongo');
	api.use('meteor');
	api.use('livedata');
	
	api.use('accounts-base'); // I think we need this to guarantee Meteor.users exists but I might be wrong, this might always exist automatically, accounts-base might just define Accounts
	api.use('accounts-password');

	api.use('howcloud:react-email', 'server');
	api.use('howcloud:collection-base');

	/* Add files */

	// Groups [used for perms etc, todo: split off user-perms/groups into a separate collection as this might not always be needed for applications...]
	
	api.add_files('groups_shared.js', ['client', 'server']);
	// api.add_files('groups_client.js', 'client'); // nothing in here right now
	api.add_files('groups_server.js', 'server');

	// Users

	api.add_files('users_shared.js', ['client', 'server']);
	api.add_files('users_client.js', 'client');
	api.add_files('users_server.js', 'server');

	/* Export */

	// Collections

	api.export('Groups', ['client', 'server']);
	api.export('Users', ['client', 'server']);

});