/**

Users
For users - we wrap the collection which meteor already makes by default
Note that all of this can be overriden by extension in the application which implements it

users: {
	groups: permission groups the user is part of
	internal: misc private data about the user which should not be sent to the client
}

**/

/**************************************************************************************/
/** Collection **/

Users = Collections.init("users", {
	collection: Meteor.users
});

Users.extendCollection({

	/** Get by Username **/

	getByUsername: function (_username, _projection) {
		return this.getByUsername_cursor(_username, _projection).fetch()[0]; // ie should just be one
	},
	getByUsername_cursor: function (_username, _projection) {
		return Users.collection.find({
			username: _username
		}, _projection);
	},

	/** Get by identfier **/
	// Users can be gotten by ids or username
	// We default to id if we cannot find by username

	getByIdentifier: function (_identifier, projection) {
		return this.getByIdentifier_cursor(_identifier, projection).fetch()[0];
	},
	getByIdentifier_cursor: function (_identifier, projection) {
		var username = this.collection.find({ // first try and find by username
			username: _identifier
		}, projection);
		
		if (username.fetch().length) return username;
		
		return this.collection.find(_identifier, projection); // by default return by id
	},

	/** Password Reset **/

	sendPasswordResetEmail: function (_email, __callback) {
		Meteor.call("userByEmail_sendPasswordResetEmail", _email, __callback);
	},

});

// DEPRECATED COMPAT FUNCTIONS
// For backwards compatibility we need to establish certain collection methods on the actual Meteor.users object as well as via extendCollection
// In time we need to move everything to using Users.[...]

Meteor.users.getById = function (_id) {
	return Users.getById(_id);
}

/**************************************************************************************/
/** Document **/

Users.extendDocument({

	/** Sorting **/

	sortValue: function () {
		return this.profile ? this.profile.name : '';
	},

	/** Profile Data **/

	firstName: function () {
		if (!this.profile) return '';
		if (!this.profile.name) return '';

		var name = this.profile.name.split(' ');
		if (!name) return ''; // this seems possible for some reason...

		return name[0];
	},

	/** Perms **/

	// User perms are attached by virtue of membership of groups

	hasPerm: function (descriptor) {
		if (!this.groups) return false;

		var find = Groups.collection.findOne({
			_id: {$in: this.groups},
			perms: descriptor
		});

		return find ? true : false;
	},
	
	/** Emails **/

	defaultEmail: function () {
		// TODO: Allow the user to set this

		if (!this.emails && (!this.services || !this.services.facebook)) return null; // we get from services.facebook manually if it's not set on the main list of user emails (due to issues we seem to be having with importing them on external service login sometimes... for some reason...)

		var email;

		if (this.emails && this.emails[0]) email = this.emails[0].address;
		if (!email && this.services && this.services.facebook) email = this.services.facebook.email;

		return email;
	},

});