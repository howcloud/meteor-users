/**

Groups
Used to group users together in logical sets and set permissions on them

**/

/**************************************************************************************/
/** Collection **/

Groups = Collections.init("groups");

/**************************************************************************************/
/** Document **/

Groups.extendDocument({

	/** Perms **/

	/*

	Perms are explicit records of authority established at the group level
	Perm descriptors may take the following format, stored in an object
	Existence of in the array implies the user has that perm (ie presence => has perm)

	[
		eg {
			type: 'topic',
			topic_id: 23r98h9,
			perm: 'view'
		}
	]

	*/

	hasPerm: function (descriptor) {
		if (!this.perms) return false;

		var rt = _.find(this.perms, function (perm) {
			return _.isEqual(perm, descriptor);
		});

		return rt ? true : false;
	},

});