/**

Groups
Server side functionality

NOTE: 'name' concept is now deprecated, we use tag

**/

/**************************************************************************************/
/** Collection **/

/* Indexes */

Groups.collection._ensureIndex({
	'perms.type': 1,
	'perms.course_id': 1, // assuming a course type here - really
	'perms.perm': 1,
});

Groups.collection._ensureIndex({
	'tag': 1,
}, {
	unique: true
});

Groups.extendCollection({

	new: function (_tag, extraData) {
		extraData = extraData || {};
		if (!extraData.tag) extraData.tag = _tag;

		var data = _.clone(extraData);

		if (extraData.tag) {
			var existing = Groups.getByTag(extraData.tag);
			if (existing) return false; // group with this tag already exists
		}

		var _group_id = Groups.collection.insert(data);

		return Groups.getById(_group_id);
	},

	assert: function (_tag, _data) {
		var group = Groups.getByTag(_tag);

		if (!group) {
			if (!_data) _data = {}
			var data = _.extend({tag: _tag}, _data);

			group = Groups.new(_tag, data);
		}

		return group;
	},

	getByTag: function (_tag) {
		return Groups.collection.findOne({tag: _tag});
	},

	clonePerms: function (match, modify) { // takes matched perms, copies, modifies and inserts
		match = match || {};

		var filter = {};
		_.each(match, function (value, key) {
			filter['perms.'+key] = value;
		});

		var groups = Groups.collection.find(filter, {
			fields: {
				_id: 1, 
				perms: {$elemMatch: match}
			}
		}).fetch();

		_.each(groups, function (group) {
			_.each(group.perms, function (perm) {
				var newPerm = _.clone(perm);
				_.extend(newPerm, modify);
				group.givePerm(newPerm);
			});
		});
	}	

});

/**************************************************************************************/
/** Document **/

Groups.extendDocument({

	/** Perms **/

	givePerm: function (descriptor) {
		if (this.hasPerm(descriptor)) return true;
		
		Groups.collection.update(this._id, {
			$push: {perms: descriptor}
		});

		return true;
	},

	takePerm: function (descriptor) {
		if (!this.hasPerm(descriptor)) return true;

		Groups.collection.update(this._id, {
			$pull: {perms: descriptor}
		});

		return true;
	},

	/** Delete **/

	delete: function () {
		Users.collection.update({}, {
			$pull: {groups: this._id}
		})

		this._super();
	},

});

/**************************************************************************************/
/** Publications **/

Meteor.publish("Group_byId", function (_group_id) {
	return Groups.collection.find(_group_id, {
		fields: {_id: 1, tag: 1}
	});
});

Meteor.publish("Groups_byIds", function (_group_ids) {
	return Groups.collection.find({_group_id: {$in: _group_ids}}, {
		fields: {_id: 1, tag: 1}
	});
});

Meteor.publish("Group_PermMatch", function (_group_id, partialDescriptor) {
	return Groups.collection.find(_group_id, {
		fields: {perms: {$elemMatch: partialDescriptor}}
	});
});

Meteor.publish("Group_Perm", function (_group_id, permDescriptor) {
	return Groups.collection.find(_group_id, {
		fields: {'perms.$': permDescriptor}
	});
});