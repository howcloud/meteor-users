/**

Users
Client side functionality

**/

/**************************************************************************************/
/** User Data **/

var subscribeData;
Meteor.startup(function () {
	Deps.autorun(function () {
        if (Meteor.userId()) subscribeData = Meteor.subscribe("ThisUser_Data", Meteor.userId());
	});
});

/* todo: deprecate and update this call */
Meteor.gotUserData = function () {
    if (subscribeData) return subscribeData.ready();
    return false;
}

/**************************************************************************************/
/** Collection **/


/**************************************************************************************/
/** Document **/

